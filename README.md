# Carrito de compras

## Instalación y configuración

1. Hacer checkout del proyecto
2. Crear una base de datos nueva para el proyecto llamada carrito
4. Instalar los paquetes de php con los comandos `composer install` 
5. Correr las migraciones con el comando `php artisan migrate`
6. Correr los seeders de la base de datos
    * Ejecutar `php artisan db:seed`
7. Ejecutar 'php artisan serve' 
