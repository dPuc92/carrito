<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'Laptop Hp 15',
            'slug' => 'laptop-hp',
            'details' => '15 pulgadas, 500gb HDD, 12GB RAM',
            'price' => 18000.00,
            'shipping_cost' => 18.00,
            'description' => 'Es una laptop hp serie 15',
            'category_id' => 1,
        ]);

        Product::create([
            'name' => 'Laptop Dell',
            'slug' => 'laptop-dell',
            'details' => '14 pulgadas, 1TB HDD, 12GB RAM',
            'price' => 16000.00,
            'shipping_cost' => 16.00,
            'description' => 'laptop dell funcional',
            'category_id' => 1,
        ]);

        Product::create([
            'name' => 'Redmi 11 Pro',
            'slug' => 'redmi-11-pro',
            'details' => '6.1 pulgadas, 128GB 6GB RAM',
            'price' => 12000.00,
            'shipping_cost' => 12.00,
            'description' => 'es el redmi 11 Pro',
            'category_id' => 2,
        ]);



    }
}
