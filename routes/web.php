<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShoppinCartController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [ShoppinCartController::class, 'tienda'])->name('tienda');
Route::get('/carrito', [ShoppinCartController::class, 'carrito'])->name('carrito.index');
Route::post('/add', [ShoppinCartController::class, 'add'])->name('carrito.store');
Route::post('/update', [ShoppinCartController::class, 'update'])->name('carrito.update');
Route::post('/remove', [ShoppinCartController::class, 'remove'])->name('carrito.remove');
Route::post('/clear', [ShoppinCartController::class, 'clear'])->name('carrito.clear');
